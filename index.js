/**
 * @format
 */

import {AppRegistry} from 'react-native';
import merda from './src/screens/merda';
import {name as appName} from './app.json';

AppRegistry.registerComponent(appName, () => merda);